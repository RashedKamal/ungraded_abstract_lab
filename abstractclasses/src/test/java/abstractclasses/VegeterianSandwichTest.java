package abstractclasses;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class VegeterianSandwichTest 
{
    @Test
    public void addAndGetFilling()
    {
        VegetarianSandwich veggie = new VegetarianSandwich();

        veggie.addFilling("cucumber");

        assertEquals(veggie.getFilling(), "cucumber");
    }

    @Test
    public void isVegeterian() 
    {
        VegetarianSandwich veggie = new VegetarianSandwich();

        assertEquals(veggie.isVegetarian(), true);
    }

    @Test
    public void isVegan() 
    {
        VegetarianSandwich veggie1 = new VegetarianSandwich();

        VegetarianSandwich veggie2 = new VegetarianSandwich();

        veggie1.addFilling("cheese");

        veggie2.addFilling("cucumber");

        assertEquals(veggie1.isVegan(), false);

        assertEquals(veggie2.isVegan(), true);
    }
}
