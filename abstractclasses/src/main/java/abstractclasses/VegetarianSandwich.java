package abstractclasses;

public class VegetarianSandwich implements ISandwich{
    private String filling;
    
    public VegetarianSandwich(){
        this.filling = "";
    }

    public void addFilling(String topping){
        String[] arr = {"chicken", "beef", "fish", "meat", "pork"};
        for (int i=0; i < arr.length; i++){
            if (topping.equals(arr[i])) {
                throw new ArrayIndexOutOfBoundsException();
            }
        }
        this.filling = this.filling+topping;
    }

    public String getFilling(){
        return this.filling;
    }

    public boolean isVegetarian(){
        return true;
    }

    public boolean isVegan(){
        if (this.filling.equals("cheese") || this.filling.equals("egg")){
            return false;
        }
        else{
            return true;
        }
    }
}