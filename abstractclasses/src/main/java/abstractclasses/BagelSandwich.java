package abstractclasses;

public class BagelSandwich implements ISandwich {

    private String filling;

    public BagelSandwich() 
    {
        this.filling = "Bacon, lettuce, tomato";
    }
    
    @Override
    public String getFilling() 
    {
        return filling;
    }

    @Override
    public void addFilling(String topping) 
    {
        this.filling = this.filling + ", " + topping;
    }

    @Override
    public boolean isVegetarian() 
    {
        throw new UnsupportedOperationException("Unimplemented method 'isVegetarian'");
    }
}
